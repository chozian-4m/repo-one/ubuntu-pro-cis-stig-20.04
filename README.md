# Ubuntu Pro STIG 20.04, hardened and maintained by Canonical

This repository contains sources to produce hardened version of Ubuntu 20.04
LTS (Focal Fossa) container image.

> **canonical/ubuntu-pro-stig**

This is the Ubuntu Pro container image maintained by Canonical. This repository receives fast security updates with LTS releases every two years. The Extended Security Maintenance (ESM) program provides 10-year security updates.     
Please read and comply with the [LICENSE](./LICENSE) file provided in the repository and the built image — more information at https://ubuntu.com/security/docker-images.

## What is Ubuntu?

Ubuntu is a Debian-based Linux operating system that runs from the desktop to the cloud, to all your internet connected things. It is the world's most popular operating system across public and OpenStack clouds. It is the number one container platform; from Docker to Kubernetes to LXD, Ubuntu can run your containers at scale. Fast, secure and straightforward, Ubuntu powers millions of PCs, and billions of containers, worldwide. Read more on the ubuntu website.

## What is Ubuntu Pro?
Ubuntu Pro is a layer providing additional services on top of Ubuntu LTS and premium open source security & compliance.
This flavour of Ubuntu Pro is hardened by the Canonical team, following both DISA STIG and CIS L2 rulesets. We made deliberate decisions regarding whether rules apply to container images (read [Hardening notes](#hardening-notes)).
Please reach out to rocks@canonical.com should you have any feedback, questions, or would like to embed our Ubuntu Pro STIG base image to ship your container images.

## Hardening notes

Applied to this image are remediations to meet subsets of DISA STIG and CIS
Level 2 configuration baselines. Some rules were excluded because they are not
suitable for container images. See shell scripts in `scripts/` directory for
applied remediations.

ca-certificate package is installed along with DoD certificate authorities which
were obtained from PKI/PKE Document Library[1]. These authorities are saved as
PEM certificate bundles in `config/certs/` directory. The `*.crt` files can be
created with the following commands:
```
wget https://dl.dod.cyber.mil/wp-content/uploads/pki-pke/zip/certificates_pkcs7_DoD.zip
unzip -x certificates_pkcs7_DoD.zip
openssl pkcs7 -in Certificates_PKCS7_v5.9_DoD/Certificates_PKCS7_v5.9_DoD.pem.p7b -print_certs >Certificates_PKCS7_v5.9_DoD.crt

wget https://dl.dod.cyber.mil/wp-content/uploads/pki-pke/zip/certificates_pkcs7_WCF.zip
unzip -x certificates_pkcs7_WCF.zip
openssl pkcs7 -in Certificates_PKCS7_v5.12_WCF/Certificates_PKCS7_v5.12_WCF.pem.p7b -print_certs >Certificates_PKCS7_v5.12_WCF.crt
```

[1] https://public.cyber.mil/pki-pke/pkipke-document-library/

## Use of DCAR Ubuntu Container Images and related scripts

This container image and its related scripts includes the Ubuntu operating system and additional packages and files (proprietary and open source) for use with Ubuntu which are published by Canonical for use by customers of Canonical’s “Ubuntu Pro”, “Ubuntu Advantage for Infrastructure” or “Ubuntu Advantage for Applications” commercial offerings (“Subscription”).

Use: This container image and its related scripts may be used on systems under your Subscription at no additional charge. This container image and its related scripts include “Canonical Content” (sometimes defined as or “Canonical Software”) as licensed under Canonical’s subscription terms for those offerings.

Distribution: Canonical has provided this container image and its related scripts for distribution via Iron Bank – DoD Centralized Artifacts Repository (“DCAR”). With a Subscription, you may access this image and its related scripts from the DCAR and deploy it for internal use. Canonical has *not* authorized redistribution of this image or its related scripts, including through layering. To request redistribution rights, please contact rocks@canonical.com. Distribution will be subject to Canonical’s IP Policy at https://ubuntu.com/legal/intellectual-property-policy and service terms as agreed with Canonical.


THIS SOFTWARE IS DISTRIBUTED WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL CANONICAL LTD. OR ANY OF ITS AFFILIATES OR LICENSORS, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THIS SOFTWARE.
