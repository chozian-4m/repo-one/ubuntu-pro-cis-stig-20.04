FROM ubuntu:focal
COPY LICENSE /LICENSE
COPY config /build/config
COPY scripts /build/scripts
RUN set -eux ;\
  export DEBIAN_FROTNEND=noninteractive ;\
  apt-get update ;\
  apt-get dist-upgrade -y ;\
  apt-get install -y ca-certificates ;\
  bash /build/scripts/fix1-stig.sh ;\
  bash /build/scripts/fix2-cis_level2_server.sh ;\
  cp /build/config/issue /etc/issue ;\
  cp /build/config/certs/*.crt /usr/local/share/ca-certificates/ ;\
  update-ca-certificates ;\
  rm -rf /build ;\
  apt-get clean ;\
  find /var/lib/apt -type f -delete ;\
  rm -f \
    /var/cache/debconf/*-old \
    /var/log/alternatives.log \
    /var/log/apt/* \
    /var/log/bootstrap.log \
    /var/log/dpkg.log \
    ;\
  find /var/log -type f -exec truncate -s0 {} \; ;\
  :
